package com.accenture.adrian.m.tabay.hcexam.Interfaces;

/**
 * Created by adriantabsy on 4/23/18.
 */

public interface IRefreshButtonClickListener {

    public void refreshWeatherList();

}
