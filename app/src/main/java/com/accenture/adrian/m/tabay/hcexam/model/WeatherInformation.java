package com.accenture.adrian.m.tabay.hcexam.model;

import com.accenture.adrian.m.tabay.hcexam.model.weather.Clouds;
import com.accenture.adrian.m.tabay.hcexam.model.weather.Coord;
import com.accenture.adrian.m.tabay.hcexam.model.weather.Main;
import com.accenture.adrian.m.tabay.hcexam.model.weather.Sys;
import com.accenture.adrian.m.tabay.hcexam.model.weather.Weather;
import com.accenture.adrian.m.tabay.hcexam.model.weather.Wind;

import java.util.ArrayList;

/**
 * Created by adriantabsy on 4/23/18.
 */

public class WeatherInformation {

    public Coord coord;
    public ArrayList<Weather> weather;
    public Main main;
    public int visibility;
    public Wind wind;
    public long dt;
    public Clouds clouds;
    public Sys sys;
    public long id;
    public String name;
    public int code;

    public Coord getCoord() {
        return coord;
    }

    public void setCoord(Coord coord) {
        this.coord = coord;
    }

    public ArrayList<Weather> getWeather() {
        return weather;
    }

    public void setWeather(ArrayList<Weather> weather) {
        this.weather = weather;
    }

    public Main getMain() {
        return main;
    }

    public void setMain(Main main) {
        this.main = main;
    }

    public int getVisibility() {
        return visibility;
    }

    public void setVisibility(int visibility) {
        this.visibility = visibility;
    }

    public Wind getWind() {
        return wind;
    }

    public void setWind(Wind wind) {
        this.wind = wind;
    }

    public long getDt() {
        return dt;
    }

    public void setDt(long dt) {
        this.dt = dt;
    }

    public Clouds getClouds() {
        return clouds;
    }

    public void setClouds(Clouds clouds) {
        this.clouds = clouds;
    }

    public Sys getSys() {
        return sys;
    }

    public void setSys(Sys sys) {
        this.sys = sys;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }
}
