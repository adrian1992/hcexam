package com.accenture.adrian.m.tabay.hcexam.fragments;

import android.app.ProgressDialog;
import android.support.v4.app.Fragment;

import com.accenture.adrian.m.tabay.hcexam.activities.BaseActivity;
import io.reactivex.Observer;

/**
 * Created by adriantabsy on 4/23/18.
 */

public class BaseFragment extends Fragment {

    protected ProgressDialog mProgressDialog;

    protected void getWeatherData(Observer<Object> observer, String city) {
        if(getActivity() instanceof BaseActivity){
            ((BaseActivity) getActivity()).getWeatherData(observer, city);
        }
    }



}
