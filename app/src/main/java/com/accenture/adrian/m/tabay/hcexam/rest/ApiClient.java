package com.accenture.adrian.m.tabay.hcexam.rest;

import com.accenture.adrian.m.tabay.hcexam.model.WeatherInformation;

import io.reactivex.Observable;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by adriantabsy on 4/23/18.
 */

public interface ApiClient {

    @GET("weather")
    Observable<WeatherInformation> getWeatherByCity(@Query("q") String city, @Query("units") String unit,
                                                    @Query("APPID") String appId);

}
