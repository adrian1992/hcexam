package com.accenture.adrian.m.tabay.hcexam.fragments;


import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.accenture.adrian.m.tabay.hcexam.Adapters.WeatherListRecyclerAdapter;
import com.accenture.adrian.m.tabay.hcexam.Interfaces.ILocationSelectedListener;
import com.accenture.adrian.m.tabay.hcexam.MainApplication;
import com.accenture.adrian.m.tabay.hcexam.R;
import com.accenture.adrian.m.tabay.hcexam.activities.WeatherDetailActivity;
import com.accenture.adrian.m.tabay.hcexam.model.WeatherInformation;
import com.accenture.adrian.m.tabay.hcexam.model.weather.Weather;
import com.accenture.adrian.m.tabay.hcexam.rest.ApiClient;
import com.accenture.adrian.m.tabay.hcexam.rest.ServiceGenerator;
import com.accenture.adrian.m.tabay.hcexam.util.Constants;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by adriantabsy on 4/23/18.
 */

public class FragCityWeather extends BaseFragment {

    @BindView(R.id.list_city_weather_rv)
    RecyclerView weatherListRv;

    @BindView(R.id.list_city_weather_ds_txt)
    TextView txtDataSource;

    View mFragmentView;
    final ArrayList<WeatherInformation> weatherInformationList = new ArrayList<>();
    final ArrayList<String> getCityToDisplay = Constants.getCities();
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        mFragmentView = inflater.inflate(R.layout.frag_list_city_weather, container, false);
        ButterKnife.bind(this, mFragmentView);
        onInit();
        return mFragmentView;
    }
    int i=0;
    public void onInit(){
        //check if there's no internet here.
        if(MainApplication.isNetworkAvailable(getActivity()))
            retrieveData();
        else
            retrieveDataFromLocal();
    }

    public void retrieveDataFromLocal(){
        txtDataSource.setText(getString(R.string.data_source, Constants.DATA_SOURCE_DB));
        RealmResults<com.accenture.adrian.m.tabay.hcexam.model.db.WeatherInformation>
                weatherInfoDbList = MainApplication.getWeatherInfoList(Realm.getDefaultInstance());
        ArrayList<WeatherInformation> weatherInfoList = new ArrayList<>();
        for(com.accenture.adrian.m.tabay.hcexam.model.db.WeatherInformation info:
                weatherInfoDbList){
            WeatherInformation infoObject = new Gson().fromJson(info.getDataJsonObject(), WeatherInformation.class);
            weatherInfoList.add(infoObject);
        }

        displayWeatherList(weatherInfoList);
    }

    public void retrieveData(){
        txtDataSource.setText(getString(R.string.data_source, Constants.DATA_SOURCE_ONLINE));
        weatherInformationList.clear();
        MainApplication.resetWeatherInfoData(Realm.getDefaultInstance());
        prepareDialog(getString(R.string.retrieve_data));
        mProgressDialog.show();
        for(String city: getCityToDisplay){
            getWeatherData(observer, city);
        }
    }

    private Observer<Object> observer = new Observer<Object>() {
        @Override
        public void onSubscribe(Disposable d) {
            //showDialog here.
        }

        @Override
        public void onNext(Object value) {
            if(value instanceof WeatherInformation){
                WeatherInformation val = (WeatherInformation) value;
                Log.d("RxJava", "On Next "+val.getName());
                Gson gson = new Gson();
                String data = gson.toJson(val);
                MainApplication.addDataWeather(Realm.getDefaultInstance(), data);
                weatherInformationList.add(val);
            }

        }

        @Override
        public void onError(Throwable e) {
            Log.d("RxJava", "Error "+e.getMessage());
        }

        @Override
        public void onComplete() {
            Log.d("RxJava", "onComplete "+weatherInformationList.size());
            if(weatherInformationList.size() == getCityToDisplay.size()){
                displayWeatherList(weatherInformationList);
                mProgressDialog.dismiss();
                Log.d("DB Data", "Size :"+MainApplication.getWeatherInfoList(Realm.getDefaultInstance()).size());
            }
        }
    };

    private void displayWeatherList(ArrayList<WeatherInformation> weatherInformationList){
        WeatherListRecyclerAdapter weatherListRecyclerAdapter =
                new WeatherListRecyclerAdapter(getActivity(), weatherInformationList, new ILocationSelectedListener() {
                    @Override
                    public void locationSelected(WeatherInformation weatherInformation) {

                        MainApplication.setWeatherInformation(weatherInformation);
                        Intent intent = new Intent(getActivity(), WeatherDetailActivity.class);
                        startActivity(intent);

                    }
                });
        weatherListRv.setLayoutManager(new LinearLayoutManager(getActivity()));
        weatherListRv.setAdapter(weatherListRecyclerAdapter);
    }

    private void prepareDialog(String message)
    {
        mProgressDialog = new ProgressDialog(getActivity());
        mProgressDialog.setCancelable(false);
        mProgressDialog.setTitle(message);
        mProgressDialog.setIndeterminate(true);

    }


}
