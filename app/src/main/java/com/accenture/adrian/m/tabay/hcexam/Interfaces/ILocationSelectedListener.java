package com.accenture.adrian.m.tabay.hcexam.Interfaces;

import com.accenture.adrian.m.tabay.hcexam.model.WeatherInformation;

/**
 * Created by adriantabsy on 4/23/18.
 */

public interface ILocationSelectedListener {
    public void locationSelected(WeatherInformation weatherInformation);
}
