package com.accenture.adrian.m.tabay.hcexam;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

import com.accenture.adrian.m.tabay.hcexam.activities.BaseActivity;
import com.accenture.adrian.m.tabay.hcexam.activities.MainActivity;
import com.accenture.adrian.m.tabay.hcexam.model.WeatherInformation;
import com.accenture.adrian.m.tabay.hcexam.rest.ApiClient;
import com.accenture.adrian.m.tabay.hcexam.rest.ServiceGenerator;
import com.accenture.adrian.m.tabay.hcexam.util.Constants;


public class SplashActivity extends BaseActivity {
    private final int SPLASH_DISPLAY_LENGTH = 2000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        MainApplication.logFBAPageView(this, this.getClass().getName());
        if(BuildConfig.FLAVOR.equalsIgnoreCase("fr")){
            setLocale(this, "fr");
        }
        setCurrentActivity(this);

        new Handler().postDelayed(new Runnable(){
            @Override
            public void run() {

                Intent mainIntent = new Intent(SplashActivity.this,MainActivity.class);
                SplashActivity.this.startActivity(mainIntent);
                SplashActivity.this.finish();
            }
        }, SPLASH_DISPLAY_LENGTH);

    }
}
