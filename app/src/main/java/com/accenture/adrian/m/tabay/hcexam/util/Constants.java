package com.accenture.adrian.m.tabay.hcexam.util;

import java.util.ArrayList;

/**
 * Created by adriantabsy on 4/23/18.
 */

public class Constants {

    public static final String WEATHER_API = "cd332abf68001ade91c409ddc0dcb922";
    public static final String BASE_URL = "http://api.openweathermap.org/data/2.5/";
    public static final String ICON_BASE_URL = "http://openweathermap.org/img/w/";
    public static final String REALM_DB_FILENAME = "weather_info_local_data";
    public static final String DATA_SOURCE_ONLINE = "Online";
    public static final String DATA_SOURCE_DB = "Local Database";

    public static final String PAGE_VIEW = "page_view";


    public static ArrayList<String> getCities(){
        ArrayList<String> cities = new ArrayList<>();
        cities.add("London");
        cities.add("Prague");
        cities.add("San Francisco");
        cities.add("Manila");
        cities.add("Cebu City");
        return cities;
    }
}
