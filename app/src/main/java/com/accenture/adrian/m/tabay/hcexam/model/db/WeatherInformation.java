package com.accenture.adrian.m.tabay.hcexam.model.db;


import io.realm.RealmObject;

/**
 * Created by adriantabsy on 4/24/18.
 */

public class WeatherInformation extends RealmObject {

    public String dataJsonObject;

    public String getDataJsonObject() {
        return dataJsonObject;
    }

    public void setDataJsonObject(String dataJsonObject) {
        this.dataJsonObject = dataJsonObject;
    }
}
