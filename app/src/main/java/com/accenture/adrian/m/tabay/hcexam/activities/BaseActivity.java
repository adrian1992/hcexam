package com.accenture.adrian.m.tabay.hcexam.activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.util.Log;

import com.accenture.adrian.m.tabay.hcexam.BuildConfig;
import com.accenture.adrian.m.tabay.hcexam.R;
import com.accenture.adrian.m.tabay.hcexam.rest.ApiClient;
import com.accenture.adrian.m.tabay.hcexam.rest.ServiceGenerator;
import com.accenture.adrian.m.tabay.hcexam.util.Constants;
import com.google.firebase.analytics.FirebaseAnalytics;

import java.util.Locale;

import butterknife.Unbinder;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by adriantabsy on 4/23/18.
 */

public class BaseActivity extends AppCompatActivity {

    protected Unbinder mUnbinder;
    ProgressDialog mProgressDialog;
    Activity currentActivity;
    private FirebaseAnalytics mFirebaseAnalytics;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
    }

    public Activity getCurrentActivity() {
        return currentActivity;
    }

    public void setCurrentActivity(Activity currentActivity) {
        this.currentActivity = currentActivity;

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mUnbinder != null) {
            mUnbinder.unbind();
        }
    }

    public void replaceFrag(int containerResId, Fragment newFrag) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(containerResId, newFrag, newFrag.getClass().toString());
        transaction.addToBackStack(null);
        transaction.commit();
        //test
    }

    public void getWeatherData(Observer<Object> observer, String city) {
        ApiClient apiClient = ServiceGenerator.createService(ApiClient.class, Constants.BASE_URL);
        apiClient.getWeatherByCity(city, "metric", Constants.WEATHER_API)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(observer);
    }

    public void prepareDialog(String message)
    {
        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setCancelable(false);
        mProgressDialog.setTitle(message);
        mProgressDialog.setIndeterminate(true);
    }

    public static void setLocale(Context context, String lang) {
        Locale myLocale = new Locale(lang);
        Resources res = context.getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.setLocale(myLocale);
        res.updateConfiguration(conf, dm);

    }

    public FirebaseAnalytics getmFirebaseAnalytics() {
        return mFirebaseAnalytics;
    }
}
