package com.accenture.adrian.m.tabay.hcexam.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.accenture.adrian.m.tabay.hcexam.MainApplication;
import com.accenture.adrian.m.tabay.hcexam.R;
import com.accenture.adrian.m.tabay.hcexam.model.WeatherInformation;
import com.accenture.adrian.m.tabay.hcexam.model.weather.Weather;
import com.accenture.adrian.m.tabay.hcexam.util.Constants;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;

import java.util.Calendar;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

/**
 * Created by adriantabsy on 4/23/18.
 */

public class WeatherDetailActivity extends BaseActivity {

    @BindView(R.id.weather_detail_icon_img)
    ImageView imgIcon;

    @BindView(R.id.weather_detail_location_txt)
    TextView txtLocation;

    @BindView(R.id.weather_detail_weather_txt)
    TextView txtWeather;

    @BindView(R.id.weather_detail_sunrise_txt)
    TextView txtSunRise;

    @BindView(R.id.weather_detail_sunset_txt)
    TextView txtSunSet;

    @BindView(R.id.weather_detail_temp_txt)
    TextView txtTemp;

    @BindView(R.id.weather_detail_min_temp_txt)
    TextView txtMinTemp;

    @BindView(R.id.weather_detail_max_temp_txt)
    TextView txtMaxTemp;

    @BindView(R.id.weather_detail_refresh_data_btn)
    Button btnRefreshData;


    private WeatherInformation weatherInformation;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weather_detail);
        MainApplication.logFBAPageView(this, this.getClass().getName());
        mUnbinder = ButterKnife.bind(this);
        setCurrentActivity(this);
        weatherInformation = MainApplication.getWeatherInformation();

        if(weatherInformation != null){
            Log.d("Icon", Constants.ICON_BASE_URL+weatherInformation.getWeather().get(0).getIcon());
            displayData();

            btnRefreshData.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    prepareDialog(getString(R.string.retrieve_data));
                    mProgressDialog.show();
                    getWeatherData(new Observer<Object>() {

                        @Override
                        public void onSubscribe(Disposable d) {

                        }

                        @Override
                        public void onNext(Object value) {
                            if(value instanceof WeatherInformation){
                                WeatherInformation weatherInformation = (WeatherInformation) value;
                                MainApplication.setWeatherInformation(weatherInformation);
                            }
                        }


                        @Override
                        public void onError(Throwable e) {
                            mProgressDialog.dismiss();
                        }

                        @Override
                        public void onComplete() {
                            displayData();
                            mProgressDialog.dismiss();
                        }
                    }, weatherInformation.getName());
                }
            });
        }



    }

    private String getDate(long timestamp){
        Calendar cal = Calendar.getInstance(Locale.ENGLISH);
        cal.setTimeInMillis(timestamp * 1000L);
        String date = DateFormat.format("dd-MM-yyyy hh:mm:ss", cal).toString();
        return date;
    }

    public void displayData(){
        Log.d("Display Data", weatherInformation.getName());
        Picasso.with(getApplicationContext()).
                load(Constants.ICON_BASE_URL+weatherInformation.getWeather().get(0).getIcon()+".png").
                into(imgIcon);
        txtLocation.setText(weatherInformation.getName());
        txtWeather.setText(weatherInformation.getWeather().get(0).getMain()+", "+
                weatherInformation.getWeather().get(0).getDescription());
        txtSunRise.setText("Sunrise : "+getDate(weatherInformation.getSys().getSunrise()));
        txtSunSet.setText("Sunset : "+getDate(weatherInformation.getSys().getSunset()));
        txtTemp.setText(getString(R.string.temperature, weatherInformation.getMain().getTemp()+""));
        txtMaxTemp.setText(getString(R.string.temperature, weatherInformation.getMain().getTempMax()+""));
        txtMinTemp.setText(getString(R.string.temperature, weatherInformation.getMain().getTempMin()+""));
    }


}
