package com.accenture.adrian.m.tabay.hcexam.model.db;

import io.realm.annotations.RealmModule;

/**
 * Created by adriantabsy on 4/24/18.
 */

@RealmModule(classes = {WeatherInformation.class})
public class WeatherDBModules {
}
