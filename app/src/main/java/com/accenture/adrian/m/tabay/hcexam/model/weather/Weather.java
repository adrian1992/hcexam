package com.accenture.adrian.m.tabay.hcexam.model.weather;

/**
 * Created by adriantabsy on 4/23/18.
 */

public class Weather {

    public String id;
    public String main;
    public String description;
    public String icon;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMain() {
        return main;
    }

    public void setMain(String main) {
        this.main = main;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }
}
