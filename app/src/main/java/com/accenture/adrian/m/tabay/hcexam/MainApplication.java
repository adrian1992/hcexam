package com.accenture.adrian.m.tabay.hcexam;

import android.app.Application;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;

import com.accenture.adrian.m.tabay.hcexam.activities.BaseActivity;
import com.accenture.adrian.m.tabay.hcexam.model.WeatherInformation;
import com.accenture.adrian.m.tabay.hcexam.model.db.WeatherDBModules;
import com.accenture.adrian.m.tabay.hcexam.util.Constants;
import com.google.firebase.analytics.FirebaseAnalytics;

import java.util.ArrayList;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmResults;

/**
 * Created by adriantabsy on 4/23/18.
 */

public class MainApplication extends Application {

    public static MainApplication instance;
    public static ArrayList<WeatherInformation> weatherInformationList;
    public static WeatherInformation weatherInformation;

    public MainApplication(){
        instance = this;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Realm.init(this);
        Realm.setDefaultConfiguration(configureRealm());
    }

    public static MainApplication getInstance() {
        // Return the instance
        return instance;
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
    }

    public static ArrayList<WeatherInformation> getWeatherInformationList(){
        return weatherInformationList;

    }

    public static void setWeatherInformationList(ArrayList<WeatherInformation> weatherInformationList){
        MainApplication.weatherInformationList = weatherInformationList;
    }

    public static WeatherInformation getWeatherInformation(){
        return weatherInformation;
    }

    public static void setWeatherInformation(WeatherInformation weatherInformation){
        MainApplication.weatherInformation = weatherInformation;
    }

    RealmConfiguration configureRealm() {
        return  new RealmConfiguration.Builder()
                .name(Constants.REALM_DB_FILENAME)
                .schemaVersion(1)
                .modules(new WeatherDBModules())
                .build();

    }

    //Realm DB operation

    public static void addDataWeather(Realm realm, String dataJsonObject){
        realm.beginTransaction();
        com.accenture.adrian.m.tabay.hcexam.model.db.WeatherInformation weatherInformation
                = realm.createObject(com.accenture.adrian.m.tabay.hcexam.model.db.WeatherInformation.class);

        weatherInformation.setDataJsonObject(dataJsonObject);
        realm.commitTransaction();
    }

    public static RealmResults<com.accenture.adrian.m.tabay.hcexam.model.db.WeatherInformation>
        getWeatherInfoList(Realm realm){
        return realm.where(com.accenture.adrian.m.tabay.hcexam.model.db.WeatherInformation.class)
                .findAll();
    }

    public static void resetWeatherInfoData(Realm realm){
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                RealmResults<com.accenture.adrian.m.tabay.hcexam.model.db.WeatherInformation>
                        result = realm.where(com.accenture.adrian.m.tabay.hcexam.model.db.WeatherInformation.class).findAll();
                result.deleteAllFromRealm();
            }
        });
    }

    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    // for every page view
    public static void logFBAPageView(BaseActivity activity, String name) {
        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, name);

        activity.getmFirebaseAnalytics().logEvent(Constants.PAGE_VIEW, bundle);
    }

}
