package com.accenture.adrian.m.tabay.hcexam.Adapters;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.accenture.adrian.m.tabay.hcexam.Interfaces.ILocationSelectedListener;
import com.accenture.adrian.m.tabay.hcexam.R;
import com.accenture.adrian.m.tabay.hcexam.model.WeatherInformation;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by adriantabsy on 4/23/18.
 */

public class WeatherListRecyclerAdapter extends RecyclerView.Adapter<WeatherListRecyclerAdapter.WeatherListViewHolder>  {

    ArrayList<WeatherInformation> weatherInformationArrayList;
    Activity activity;
    ILocationSelectedListener iLocationSelectedListener;

    public WeatherListRecyclerAdapter(Activity activity,
                                       ArrayList<WeatherInformation> weatherInformations,
                                      ILocationSelectedListener iLocationSelectedListener){
        this.weatherInformationArrayList =weatherInformations;
        this.activity = activity;
        this.iLocationSelectedListener = iLocationSelectedListener;
    }

    @Override
    public WeatherListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View inflatedView = LayoutInflater.from(parent.getContext()).inflate(R.layout.city_weather_item, parent, false);
        return new WeatherListViewHolder(inflatedView);
    }


    @Override
    public void onBindViewHolder(WeatherListViewHolder holder, int position) {
        holder.bind(weatherInformationArrayList.get(position));
    }

    @Override
    public int getItemCount() {
        return weatherInformationArrayList.size();
    }


    public class WeatherListViewHolder extends RecyclerView.ViewHolder
    implements View.OnClickListener{

        View mView;

        @BindView(R.id.city_weather_item_location)
        TextView txtLocation;

        @BindView(R.id.city_weather_item_temp)
        TextView txtTemp;

        @BindView(R.id.city_weather_item_weather)
        TextView txtWeather;

        public WeatherListViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            mView =itemView;
            mView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if(view.getTag() instanceof WeatherInformation){
                iLocationSelectedListener.locationSelected((WeatherInformation) view.getTag());
            }
        }

        public void bind(WeatherInformation weatherInformation){
            mView.setTag(weatherInformation);
            String weather = "";
            for(int i=0; i<weatherInformation.getWeather().size(); i++){
                weather += weatherInformation
                        .getWeather().get(i).getDescription()+" ";
            }

            txtWeather.setText(weather);
            txtLocation.setText(weatherInformation.getName());
            txtTemp.setText("Temperature: "+activity.getString(R.string.temperature, weatherInformation.getMain().getTemp()));

        }
    }

}
