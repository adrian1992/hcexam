package com.accenture.adrian.m.tabay.hcexam.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.accenture.adrian.m.tabay.hcexam.R;
import com.accenture.adrian.m.tabay.hcexam.activities.MainActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by adriantabsy on 4/23/18.
 */

public class FragButtonRefresh extends Fragment implements View.OnClickListener {

    protected View mFragmentView;
    protected MainActivity parentActivity;

    @BindView(R.id.refresh_button_btn)
    Button btnRefresh;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        mFragmentView = inflater.inflate(R.layout.frag_refresh_button, container, false);
        ButterKnife.bind(this, mFragmentView);
        parentActivity = (MainActivity) getActivity();
        btnRefresh.setOnClickListener(this);
        return mFragmentView;
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.refresh_button_btn:

                parentActivity.refreshWeatherList();

                break;
            default:
                break;
        }
    }
}
