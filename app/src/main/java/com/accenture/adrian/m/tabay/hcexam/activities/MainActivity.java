package com.accenture.adrian.m.tabay.hcexam.activities;

import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.util.DisplayMetrics;
import android.util.Log;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.accenture.adrian.m.tabay.hcexam.BuildConfig;
import com.accenture.adrian.m.tabay.hcexam.Interfaces.IRefreshButtonClickListener;
import com.accenture.adrian.m.tabay.hcexam.MainApplication;
import com.accenture.adrian.m.tabay.hcexam.R;
import com.accenture.adrian.m.tabay.hcexam.fragments.FragButtonRefresh;
import com.accenture.adrian.m.tabay.hcexam.fragments.FragCityWeather;

import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by adriantabsy on 4/23/18.
 */

public class MainActivity extends BaseActivity implements IRefreshButtonClickListener {

    @BindView(R.id.main_screen_refresh_btn_fl)
    FrameLayout flRefreshButton;

    @BindView(R.id.main_screen_weather_list_fl)
    FrameLayout flWeatherList;

    FragButtonRefresh fragButtonRefresh;
    FragCityWeather fragCityWeather;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_screen);
        MainApplication.logFBAPageView(this, this.getClass().getName());
        mUnbinder = ButterKnife.bind(this);
        setCurrentActivity(this);
        Log.d("Build type", BuildConfig.FLAVOR);
        if(BuildConfig.FLAVOR.equalsIgnoreCase("fr")){
            setLocale(this, "fr");
        }
        onInit();
    }

    public void onInit(){
        fragButtonRefresh = new FragButtonRefresh();
        fragCityWeather = new FragCityWeather();

        replaceFrag(R.id.main_screen_refresh_btn_fl, fragButtonRefresh);
        replaceFrag(R.id.main_screen_weather_list_fl, fragCityWeather);

    }

    @Override
    public void refreshWeatherList() {
        if(MainApplication.isNetworkAvailable(this)) {
            fragCityWeather.retrieveData();
        }else{
            fragCityWeather.retrieveDataFromLocal();
            Toast.makeText(this, getString(R.string.no_available_network), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
