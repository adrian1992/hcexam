package com.accenture.adrian.m.tabay.hcexam.model.weather;

/**
 * Created by adriantabsy on 4/23/18.
 */

public class Clouds {
    public int all;

    public int getAll() {
        return all;
    }

    public void setAll(int all) {
        this.all = all;
    }
}
